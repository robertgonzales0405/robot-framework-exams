*** Settings ***
Documentation    Suite description


*** Keywords ***
Open Chrome Browser
    [Arguments]    ${url}
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    disable-extensions
    Call Method    ${chrome options}   add_argument    start-maximized
    Create Webdriver    Chrome    chrome_extension_disabled    chrome_options=${chrome_options}  executable_path=${CURDIR}\\..\\chromedriver.exe
    Go To    ${url}

Run Browser
    Run Keyword    Open Chrome Browser    ${URL}


Set
    [Arguments]  ${locator}  ${text}
    wait until element is visible  ${locator}  20
    SeleniumLibrary.Input Text  ${locator}  ${text}

Click
    [Arguments]    ${locator}
    Click Element  ${locator}
    Sleep  1
