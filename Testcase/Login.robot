*** Settings ***
Documentation    Suite description

Library  SeleniumLibrary
Library    Process
Resource  ../Keyword/login_keywords.robot
Resource  ../Variables/Variables.robot

Test Setup  Run Keywords     Run Browser
Test Teardown  Run Keywords  Close Browser

*** Test Cases ***
Scenario1 - Ivalid email address format
    [Tags]  negative_scenario
    Set  ${usernameField}  ${user}
    Click   ${loginButton}
    wait until element is visible  ${validationMessage1}  20
    sleep  5

Scenario2 - Empty Fields
    [Tags]  negative_scenario
    Click   ${loginButton}
    wait until element is visible  ${validationMessage2}  20
    wait until element is visible  ${validationMessage3}  20
    sleep  5

Scenario3 - Unregister Account
    [Tags]  negative_scenario
    Set  ${usernameField}  ${validUser}
    Set  ${passwordField}   ${password}
    Click   ${loginButton}
    wait until element is visible  ${validationMessage4}  20
    sleep  5


Scenario4 - Invalid email in forgot password
    [Tags]  negative_scenario
    Click   ${lost_passwordButton}
    Click   ${recover_my_accessButton}
    wait until element is visible  ${validationMessage2}  20
    sleep  5





