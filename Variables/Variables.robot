*** Settings ***
Documentation    Suite description


*** Variables ***
#Login Page Xpath
${usernameField}            //input[@type='text'and @name='email']
${usernameField2}           //input[@class='form-control required email preventSpace']
${passwordField}            //input[@type='password'and @name='password']
${loginButton}              //button[contains(@type,'submit') and contains(text(),'Login')]
${recover_my_accessButton}                             //button[contains(@type,'submit') and contains(text(),'Recover my access')]
${user}                     invalid email
${validUser}                testemail@gmail.com
${password}                 testpassword
${URL}                      https://login.optimyapp.com/
${validationMessage1}       //span[contains(text(),'Please enter a valid email address (e.g.: john.smith@gmail.com).') and contains(@for,'email')]
${validationMessage2}       //span[contains(text(),'This field is required.') and contains(@for,'email')]
${validationMessage3}       //span[contains(text(),'This field is required.') and contains(@for,'password')]
${validationMessage4}       //div[contains(text(),'The email address or password is incorrect.') and contains(@id,'login-invalid')]
${lost_passwordButton}      //a[contains(text(),'Lost password?')]

